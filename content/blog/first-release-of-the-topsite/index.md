---
title: First release of the topsite
date: "2019-10-10T23:56:23.648Z"
description: A blog on the first release of an open source application i've developed.
---

Today is a big day for me, for the first time I've been able to release an application as open source, I've done bits
and pieces in Open Source work but nothing as big as this.

# Topsite

It's a good feeling being able to go on Github and tag a first release of something,
today I got that honor and it feels great. I've finally made it from nothing to something in terms
of building something open source.

One of my first projects on Github was a simple topsite script to show off what I knew, suffice
to say it wasn't a lot. But now I relive that but the difference being I did it not just for myself.
We have a community over at [PBBG](https://pbbg.com), you can also visit the [Discord](https://pbbg.chat), we are
a community of developers and players that are based around Persistent Browser Based Games, or PBBG for short.

I started this as a community project, our first of many I hope. Though there was only one other contributor bar myself
it still felt good, I hope others join in on the action in future.

# What is it?
The topsite is an application to host a listing site, currently it's heavily revolved around games, but I intend to
generalise things more in the future in terms of naming. Anyhow, digressing. It's basically a website in which you can list your game
people can vote for it from within their own games and gain rewards through an incentive program.

The idea was to replace the older more defunct [top-pbbg.com](https://top-pbbg.com/) with something new and fresh, something easy to develop 
and maintain, and that mission has been accomplished!

There was a lot of technical debt with the old one, to the point where links were broken, no votes were coming in (plenty going out) and 
developing on it would be a nightmare. We chose [Laravel](https://laravel.com) as our base framework and stuck
with good old blade templates instead of opting for [Vue](https://vuejs.org) it meant faster development, and in reality
we don't need what Vue has to offer.

Now we have something stable to work from in the future.

# What's next?

Tests tests tests and refactoring. Version 1 has been a great start but it lacking a lot especially in... you guessed it
the tests department. It has a basic test suite, but other than that it's pretty useless.

There are certain parts that also need refactoring such as callbacks from votes in are synchronous when they should be outsourced
to jobs. Generalising from Gaming to Listings more is also needed.

There are also a whole bunch of features that still need to be developed along with our own theme to make it more homely, but all in due time.

# Thanks

Thanks to [WaveHack](https://github.com/WaveHack) for his help and to [Vince](https://github.com/foohonpie) for letting me lead this on behalf of [PBBG](https://pbbg.com) and all our [contributors](https://github.com/pbbg/topsite/graphs/contributors) and thank you for reading