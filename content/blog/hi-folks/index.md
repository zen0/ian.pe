---
title: New Beginnings
date: "2019-06-04T16:19:23.648Z"
description: My new blog, a new start.
---

Decided to start a new blog since I've been using the domain for over 6 years.

# GatsbyJS

Gatsby is a cool static site generator I was recommend too by someone on our Telegram group for makers. I wanted 
something simple that allows me to just write without needing to deal with any bugs or code. Go Gatsby was a simple choice.

It also gives me the flexibility to build on top of it should I need to, such as adding a portfolio page in future, or current
projects etc.

Hosted on [Netlify](https://netlify.com) it's pretty easy to get going.