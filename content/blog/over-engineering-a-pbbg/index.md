---
title: Over Engineering a PBBG
date: "2019-11-15T20:27:10.648Z"
description: Over engineering PBBG's instead of getting an MvP out the door.
---

Hot topic today, over engineering a PBBG. As a little background, I run a game that brings in mid 5 figures in revenue
 each year, so this is what I've learnt from that. I would also like to 
 mention I inherited a legacy code base so that definitely effects some of my decisions.

On the [PBBG Discord](https://pbbg.chat) I've noticed a lot of great discussions about 
how to do something or whether something should be done. Or more over 
promoting doing certain things. So here is a few bits of food for thought

## Tests
One of the main ones is tests.
My game has 0 tests, it was built for some ancient version of PHP way back when, I'd hazard a guess and say pre 5.0. In the last year I have bought it up to 7.3, soon to be 7.4.
Every decent developer would advise, write tests. I'd say don't. I guess that makes me a terrible developer. There is a time and a place for tests and in my case I really don't feel the need for them.
If a bug comes up, I fix it, the players I have are understanding enough to not warrant potentially hundreds of hours writing tests.

I've also noticed that some people suggest writing tests despite not having anything out there. I personally would much rather see something than need to wait another few months so the developer can write some tests.
Don't be afraid of letting your players be the testers. We don't live in a perfect world. Your BETA doesn't need to be perfect.

But please, make sure you actually log your errors, we use [Sentry](https://sentry.io/welcome/) which handles a whole bunch of apps,
 you can use it for frontend, backend, android apps, any applications really in most languages. 

## MvP

Get something out the damn door. Stop over thinking and over complicating things. There is a slim chance your game will
massively succeed, and by that, I mean make a living off of it. You need to get players impressions as soon as you can
even if you have a minimal base with a minimal feature set. Because if you can't get any players, or players don't like it
you can easily pivot without worrying about spending thousands of hours building something people won't want.

So get something out there for people to see and use

## Storing IP's

IP's are a crucial part of an anti cheat especially when it comes to duping. Yes there are ways to get around this,
using a proxy or VPN, TOR even. But not everyone playing your game is a wizard or even understands what an IP is.
Chances are you're reading this and you're a developer, remember, not everyone else is! We log everything against an IP,
any action taken, be it selling something on the marketplace, shooting someone, even things like transferring money into
 your bank. We log it, IP, name, previous amount of items if applicable.
 
 You can change your IP after logging in, so remember to record it against actions, rather than just a check on login. Attaching
 an IP to a session is also helpful, that way they get instantly logged out if they change IP. There are many benefits to knowing this.
 
## Conclusion

Stop over thinking technical things and start getting an MvP out there for the world to see. People expect hiccups when you launch 
it's only natural.

Get developing, get something out, iterate over time rather than aiming for perfection off the bat!